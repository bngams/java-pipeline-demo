package fr.aelion.agedi.springdemo.intro.utils;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Calculator {

    public Integer calculate(Integer a, Integer b, String operation) throws ScriptException {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("js");
        Object result = engine.eval(a + operation + b);
        return Integer.parseInt(result.toString());
    }
}

package fr.aelion.agedi.springdemo;

import io.gatling.javaapi.core.CoreDsl;
import io.gatling.javaapi.core.ScenarioBuilder;
import io.gatling.javaapi.core.Simulation;
import io.gatling.javaapi.http.HttpDsl;
import io.gatling.javaapi.http.HttpProtocolBuilder;

import static io.gatling.javaapi.core.OpenInjectionStep.atOnceUsers;

public class GatlingSimulation extends Simulation {

    // Configure http client
    // Protocol Definition
    HttpProtocolBuilder httpProtocol = HttpDsl.http
            .baseUrl("http://localhost:8282")
            .acceptHeader("text/html,application/json")
            .userAgentHeader("Gatling/Performance Test");

    // Scenario
    ScenarioBuilder scn = CoreDsl.scenario("My first Gatling Scenario")
            .exec(
                    HttpDsl.http("call hello").get("/intro/hello")
            );

    // Simulation
    public GatlingSimulation() {
        // launch gatling by configuring the simulation
        this.setUp(
                // scenario(s) to execute
                scn.injectOpen(atOnceUsers(15))
        ).protocols(httpProtocol); // load our http config (our http client)
    }

}

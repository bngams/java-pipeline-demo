package fr.aelion.agedi.springdemo.intro.utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.script.ScriptException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CalculatorTest {

    Calculator calc;

    @BeforeEach
    public void init() {
        this.calc = new Calculator();
    }

    // @Test
    public void calculateAdditionTest() throws ScriptException {
        Integer wantedResult = 4;
        Integer a = 2;
        Integer b = 2;
        String operation = "+";
        Integer result = this.calc.calculate(a, b, operation);
        assertEquals(wantedResult, result);
    }

    // @Test
    public void calculateAdditionTestShouldTriggerIfBadParam() throws ScriptException {
        // ....
    }
}
